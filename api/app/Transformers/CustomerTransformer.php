<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Customer;

class CustomerTransformer extends TransformerAbstract
{

    public function transform(Customer $item)
    {
        return [
            'id'            => (int)$item->id,
            'email'         => $item->email,
            'lastname'      => $item->lastname,
            'firstname'     => $item->firstname,
            'phone'         => $item->phone,
			'address'       => $item->address,
			'sex'       	=> $item->sex,
            'active'        => (boolean)$item->active,
            'created_at'    => $item->created_at,
            'updated_at'    => $item->updated_at,
        ];
    }
}


